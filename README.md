# ETL ETROC module, rev 2.1
### Initially converted from Altium design v1.0b and reworked
## Changes:

1. The previous version used dual cellphone-style connectors for attachment of the Module PCB to the RB PCB. The connector manufacturer (I-PEX) has clearly indicated that such an arrangement is not supported, since these connectors have very low positioning tolerance. It will be hard to install them precisely enough relative to each other. We have converted the design to use a single connector for board-to-board connection.
2. Waveform Sampler pins (I2C, RESET, and EN) are connected as recommended. WS should be usable on these boards.
3. Bias voltage was previously supplied via 4 separate contacts to each of the ETROCs individually. This is now modified to have only one BV contact per ETROC module.
4. We have increased the HV clearances to ~1000V on RB, and around 700V on ETROC module. (There's no way to provide 1000V clearance on the module).
5. RB design has the power supply section built in. It still has an option of being powered from an external power board. That will let us test both options and decide which one works better.
6. On ETROC module, Analog and Digital power planes are separated. Analog plane is common for all 4 chips. Digital plane is split in half (2+2 chips) to help balance DC-DC load on the RB. On RB, Analog and Digital power inputs of ETROC modules are fed from separate DC-DC converters.
7. For DC-DC converters feeding Analog power inputs, we added LDOs at the outputs. This feature is for testing only. We are aware that we cannot use LDOs in the production system. We have an option for removing these LDOs and feeding Analog planes directly from bPOL12 converters. Having LDOs will let us evaluate how much noise is injected when Analog lines are fed directly from bPOL12s.
8. Analog monitoring is now done using MUX64 analog multiplexor.
9. The VREF measurement schematics is modified to avoid affecting VREF value inside the ETROC chips. In particular, there are no permanent resistive dividers attached to VREF outputs. VREF outputs go directly to the MUX64 inputs. When these inputs are disconnected, these connections have very small leakage current that does not affect VREFs. When they are connected for measurement, there's a current into the divider at the MUX64 output. Consequently, the act of measuring VREF affects VREF itself, so it has limited precision. Therefore, these measurements should only be done when data is not being taken.
10. For testing purposes only, we've added a bypassable buffer at the output of MUX64. We are aware that it cannot be used in the production system. This will be used for precise measurements of the VREF at a test stand.
11. We've also added a Bias Voltage monitoring using a HV divider connected to one of the MUX64 inputs.
